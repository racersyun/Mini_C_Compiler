//2010305042 �����
#define _CRT_SECURE_NO_WARNINGS
#define PS_SIZE 200

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "minic.tbl"
#include "scanner.c"
#include "parser.cpp"
#include "SymTab.c"

FILE *astFile;
FILE *ucodeFile;
FILE *sourceFile;
int labelcount = 0;
int returnWithValue = 0;
int lvalue = 0;
int assigncount = 0;

void codeGen(Node *ptr);
void processDeclaration(Node *ptr);
void processSimpleVariable(Node *ptr, int typeSpecifier, int typeQualifier);
void processArrayVariable(Node *ptr, int typeSpecifier, int typeQualifier);
void processOperator(Node *ptr);
void processStatement(Node *ptr);
void processCondition(Node *ptr);
void processFuncHeader(Node *ptr);
void processFunction(Node* ptr);
int typeSize(int typeSpecifier);
int checkPredefined(Node* ptr);
void genLabel(char* label);
void emitLabel(char* label);
void icg_error(int err);

void main(int argc, char *argv[]){
	char fileName[30];
	Node *root;

	printf(" *** Start of MiniC Compiler\n");
	if (argc != 2){
		printf("Usage : Compiler.exe [Source File]\n");
		icg_error(1);
		exit(1);
		}
	strcpy(fileName, argv[1]);
	printf("  * Source file name : %s\n", fileName);

	if ((sourceFile = fopen(fileName, "r")) == NULL){
		printf("%s open failed!\n", fileName);
		icg_error(2);
		exit(1);
	}

	astFile = fopen(strcat(strtok(fileName, "."), ".ast"), "w");
	ucodeFile = fopen(strcat(strtok(fileName, "."), ".uco"), "w");

	printf(" === Start of Parser\n");
	root = parser();
	printTree(root, 0);
	codeGen(root);

	printf(" *** End of MiniC Compiler\n");
}

void codeGen(Node *ptr){
	Node *p;
	int globalSize;
	int stIndex;

	initSymbolTable();

	stIndex = insert("write", 0, 1, 1, 1, 1, 0);
	stIndex = insert("read", 0, 1, 1, 1, 1, 0);

	for (p = ptr->son; p; p = p->brother){
		if (p->token.number == DCL)
			processDeclaration(p->son);
		else if (p->token.number == FUNC_DEF)
			processFuncHeader(p->son);
		else
			icg_error(3);
	}

	globalSize = offset - 1;
	genSym(base);

	for (p = ptr->son; p; p = p->brother){
		if (p->token.number == FUNC_DEF){
			processFunction(p);
		}
	}

	emit1("bgn", globalSize);
	emit0("ldp");
	emitJump("call", "main");
	emit0("end");
}

void processDeclaration(Node *ptr){
	int typeSpecifier, typeQualifier;
	Node *p, *q;

	if (ptr->token.number != DCL_SPEC)
		icg_error(4);

	typeSpecifier = INT_TYPE;
	typeQualifier = VAR_TYPE;
	p = ptr->son;

	while (p){
		if (p->token.number == INT_NODE)
			typeSpecifier = INT_TYPE;
		else if (p->token.number == CONST_NODE)
			typeQualifier = CONST_TYPE;
		else{
			printf("Not Yet Implemented\n");
			return;
		}
		p = p->brother;
	}
	p = ptr->brother;
	if (p->token.number != DCL_ITEM)
		icg_error(5);

	while (p){
		q = p->son;
		switch (q->token.number){
		case SIMPLE_VAR:
			processSimpleVariable(q, typeSpecifier, typeQualifier);
			break;
		case ARRAY_VAR:
			processArrayVariable(q, typeSpecifier, typeQualifier);
			break;
		default:
			printf("Error in SIMPLE_VAR or ARRAY_VAR\n");
			break;
		}
		p = p->brother;
	}
}

void processSimpleVariable(Node *ptr, int typeSpecifier, int typeQualifier){
	Node *p = ptr->son;
	Node *q = ptr->brother;
	int stIndex, size, initialValue;
	int sign = 1;

	if (ptr->token.number != SIMPLE_VAR)
		printf("Error in SIMPLE_VAR\n");

	if (typeQualifier == CONST_TYPE){
		if (q == NULL){
			printf("%s must have a constant value\n", ptr->son->token.value.id);
			return;
		}
		if (q->token.number == UNARY_MINUS){
			sign = -1;
			q = q->son;
		}
		initialValue = sign*q->token.value.num;

		stIndex = insert(p->token.value.id, typeSpecifier, typeQualifier, 0, 0, 0, initialValue);
	}
	else{
		size = typeSize(typeSpecifier);
		stIndex = insert(p->token.value.id, typeSpecifier, typeQualifier, base, offset, size, 0);
		offset += size;
	}
}

void processArrayVariable(Node *ptr, int typeSpecifier, int typeQualifier){
	Node *p = ptr->son;
	int stIndex, size;

	if (ptr->token.number != ARRAY_VAR){
		printf("Error in ARRAY_VAR\n");
		return;
	}
	if (p->brother == NULL)
		printf("Array size must be specified\n");
	else
		size = p->brother->token.value.num;
	size *= typeSize(typeSpecifier);

	stIndex = insert(p->token.value.id, typeSpecifier, typeQualifier, base, offset, size, 0);
	offset += size;
}

void processOperator(Node *ptr){
	int stIndex;
	Node *lhs = ptr->son, *rhs = ptr->son->brother;

	switch (ptr->token.number){
	case ASSIGN_OP:{
					   if (lhs->noderep == nonterm){
						   lvalue = 1;
						   processOperator(lhs);						   
						   lvalue = 0;
					   }

					   if (rhs->noderep == nonterm)
						   processOperator(rhs);
					   else
						   rv_emit(rhs);

					   if (lhs->noderep == terminal){
						   stIndex = lookup(lhs->token.value.id);
						   if (stIndex == -1){
							   printf("undefined variable : %s\n", lhs->token.value.id);
							   return;
						   }
						   emit2("str", symbolTable[stIndex].base, symbolTable[stIndex].offset);
						   if (lhs->brother->token.number == SUB)
							   rv_emit(ptr->son);
					   }
					   else
						   emit0("sti");

					   break;
	}
	case ADD_ASSIGN: case SUB_ASSIGN: case MUL_ASSIGN:
	case DIV_ASSIGN: case MOD_ASSIGN:{
						 int nodeNumber = ptr->token.number;

						 ptr->token.number = ASSIGN_OP;

						 if (lhs->noderep == nonterm){
							 lvalue = 1;
							 processOperator(lhs);
							 lvalue = 0;
						 }

						 ptr->token.number = nodeNumber;

						 if (lhs->noderep == nonterm)
							 processOperator(lhs);
						 else
							 rv_emit(lhs);

						 if (rhs->noderep == nonterm)
							 processOperator(rhs);
						 else
							 rv_emit(rhs);

						 switch (ptr->token.number){
						 case ADD_ASSIGN:
							 emit0("add");
							 break;
						 case SUB_ASSIGN:
							 emit0("sub");
							 break;
						 case MUL_ASSIGN:
							 emit0("mult");
							 break;
						 case DIV_ASSIGN:
							 emit0("div");
							 break;
						 case MOD_ASSIGN:
							 emit0("mod");
							 break;
						 }

						 if (lhs->noderep == terminal){
							 stIndex = lookup(lhs->token.value.id);
							 if (stIndex == -1){
								 printf("undefined variable : %s\n", lhs->son->token.value.id);
								 return;
							 }
							 emit2("str", symbolTable[stIndex].base, symbolTable[stIndex].offset);
						 }
						 else
							 emit0("sti");
						 break;
	}
	case ADD: case SUB: case MUL: case DIV: case MOD:
	case EQ: case NE: case GT: case LT: case GE:
	case LE: case LOGICAL_AND: case LOGICAL_OR:{

				 if (lhs->noderep == nonterm)
					 processOperator(lhs);
				 else
					 rv_emit(lhs);

				 if (rhs->noderep == nonterm)
					 processOperator(rhs);
				 else
					 rv_emit(rhs);

				 switch (ptr->token.number){
				 case ADD:
					 emit0("add");
					 break;
				 case SUB:
					 emit0("sub");
					 break;
				 case MUL:
					 emit0("mult");
					 break;
				 case DIV:
					 emit0("div");
					 break;
				 case MOD:
					 emit0("mod");
					 break;
				 case EQ:
					 emit0("eq");
					 break;
				 case NE:
					 emit0("ne");
					 break;
				 case GT:
					 emit0("gt");
					 break;
				 case LT:
					 emit0("lt");
					 break;
				 case GE:
					 emit0("ge");
					 break;
				 case LE:
					 emit0("le");
					 break;
				 case LOGICAL_AND:
					 emit0("and");
					 break;
				 case LOGICAL_OR:
					 emit0("or");
					 break;
				 }
				 break;
	}
	case UNARY_MINUS: case LOGICAL_NOT:{
						  Node *p = ptr->son;
						  if (p->noderep == nonterm)
							  processOperator(p);
						  else
							  rv_emit(p);

						  switch (ptr->token.number){
						  case UNARY_MINUS:
							  emit0("neg");
							  break;
						  case LOGICAL_NOT:
							  emit0("notop");
							  break;
						  }
						  break;
	}
	case PRE_INC: case PRE_DEC:	case POST_INC: case POST_DEC:{
					  Node *p = ptr->son, *q;
					  int amount = 1;

					  if (p->noderep == nonterm)
						  processOperator(p);
					  else
						  rv_emit(p);

					  q = p;

					  while (q->noderep != terminal)
						  q = q->son;

					  if (!q || (q->token.number != tident)){
						  printf("increment/decrement operators can not be applied in expression\n");
						  return;
					  }
					  stIndex = lookup(q->token.value.id);
					  if (stIndex == -1)
						  return;

					  switch (ptr->token.number){
					  case PRE_INC:
						  emit0("inc");
						  break;
					  case PRE_DEC:
						  emit0("dec");
						  break;
					  case POST_INC:
						  emit0("inc");
						  break;
					  case POST_DEC:
						  emit0("dec");
						  break;
					  }

					  if (p->noderep == terminal){
						  stIndex = lookup(p->token.value.id);
						  if (stIndex == -1)
							  return;
						  emit2("str", symbolTable[stIndex].base, symbolTable[stIndex].offset);
					  }
					  else if (p->token.number == INDEX){
						  lvalue = 1;
						  processOperator(p);
						  lvalue = 0;
						  emit0("swp");
						  emit0("sti");
					  }
					  else
						  printf("error in increment/decrement operators\n");
					  break;
	}
	case INDEX:{
				   Node *indexExp = ptr->son->brother;

				   if (indexExp->noderep == nonterm)
					   processOperator(indexExp);
				   else
					   rv_emit(indexExp);

				   stIndex = lookup(ptr->son->token.value.id);

				   if (stIndex == -1){
					   printf("undefined variable : %s\n", ptr->son->token.value.id);
					   return;
				   }
				   emit2("lda", symbolTable[stIndex].base, symbolTable[stIndex].offset);
				   emit0("add");

				   if (!lvalue){
					   emit0("ldi");
				   }
				   break;
	}
	case CALL:{
				  Node *p = ptr->son;
				  char *functionName;
				  int noArguments;

				  if (checkPredefined(p))
					  break;

				  functionName = p->token.value.id;
				  stIndex = lookup(functionName);

				  if (stIndex == -1)
					  break;

				  noArguments = symbolTable[stIndex].width;
				  emit0("ldp");
				  p = p->brother;

				  while (p){
					  if (p->noderep == nonterm)
						  processOperator(p);
					  else if (strcmp(ptr->son->token.value.id, "read") == 0){
						  rv_emit2(ptr);
					  }
					  else
						  rv_emit(p);
					  noArguments--;
					  p = p->brother;
				  }

				  if (noArguments > 0)
					  printf("%s : too few actual arguments", functionName);
				  if (noArguments < 0)
					  printf("%s : too many actual arguments", functionName);
				  emitJump("call", ptr->son->token.value.id);
				  break;
	}
	}
}

int typeSize(int typeSpecifier){
	if (typeSpecifier == INT_TYPE)
		return 1;
	else
		return 0;
}

void processStatement(Node *ptr){
	Node *p;
	char label[LABEL_SIZE];
	char label1[LABEL_SIZE], label2[LABEL_SIZE];
	switch (ptr->token.number){
	case COMPOUND_ST:
		p = ptr->son->brother;
		p = p->son;
		while (p){
			processStatement(p);
			p = p->brother;
		}
		break;
	case EXP_ST:
		if (ptr->son != NULL)
			processOperator(ptr->son);
		break;
	case RETURN_ST:
		if (ptr->son != NULL){
			returnWithValue = 1;
			p = ptr->son;
			if (p->noderep == nonterm)
				processOperator(p);
			else
				rv_emit(p);
			emit0("retv");
		}
		else
			emit0("ret");
		break;
	case IF_ST:
		genLabel(label);
		processCondition(ptr->son);
		emitJump("fjp", label);
		processStatement(ptr->son->brother);
		emitLabel(label);

		break;
	case IF_ELSE_ST:
		genLabel(label1);
		genLabel(label2);
		processCondition(ptr->son);
		emitJump("fjp", label1);
		emitLabel(label1);
		processStatement(ptr->son->brother->brother);
		emitLabel(label2);

		break;
	case WHILE_ST:
		char label1[LABEL_SIZE], label2[LABEL_SIZE];

		genLabel(label1);
		genLabel(label2);
		emitLabel(label1);
		processCondition(ptr->son);
		emitJump("fjp", label2);
		processStatement(ptr->son->brother);
		emitJump("ujp", label1);
		emitLabel(label2);
		break;
	default:
		printf("Not yet implemented\n");
	}
}

void processCondition(Node *ptr){
	if (ptr->noderep == nonterm)
		processOperator(ptr);
	else
		rv_emit(ptr);
}

void processFuncHeader(Node *ptr){
	int noArguments, returnType;
	int stIndex;
	Node *p;

	if (ptr->token.number != FUNC_HEAD)
		printf("Error in processFuncHeader\n");
	p = ptr->son->son;

	while (p){
		if (p->token.number == INT_NODE)
			returnType = INT_TYPE;
		else if (p->token.number == VOID_NODE)
			returnType = VOID_TYPE;
		else
			printf("invalid function return type\n");
		p = p->brother;
	}

	p = ptr->son->brother->brother;
	p = p->son;

	noArguments = 0;
	while (p){
		noArguments++;
		p = p->brother;
	}

	stIndex = insert(ptr->son->brother->token.value.id, returnType, FUNC_TYPE, 1, 0, noArguments, 0);
}

void processFunction(Node* ptr){
	Node *p, *q, *r;

	base++;

	if (ptr->token.number != FUNC_DEF)
		printf("error in processFunction\n");

	//1
	p = ptr->son->son->brother->brother;
	p = p->son;
	while (p){
		processDeclaration(p->son);
		p = p->brother;
	}
	//2
	q = ptr->son->brother->son;
	q = q->son;
	while (q){
		processDeclaration(q->son);
		q = q->brother;
	}
	dumpSymbolTable();
	//3
	//printf("%s       fun %6d %6d %6d\n",ptr->son->son->brother->token.value.id, offset - 1, base, base);
	printf("%s", ptr->son->son->brother->token.value.id);
	for (int siz = 0; siz < 11-strlen(ptr->son->son->brother->token.value.id); siz++)
		printf(" ");
	printf("fun %6d %6d %6d\n", offset - 1, base, base);
	//fprintf(ucodeFile, "%s       fun %6d %6d %6d\n", ptr->son->son->brother->token.value.id, offset - 1, base, base);
	fprintf(ucodeFile, "%s", ptr->son->son->brother->token.value.id);
	for (int siz = 0; siz < 11 - strlen(ptr->son->son->brother->token.value.id); siz++)
		fprintf(ucodeFile," ");
	fprintf(ucodeFile, "fun %6d %6d %6d\n", offset - 1, base, base);
	genSym(base);

	//4
	r = ptr->son->brother;
	processStatement(r);

	//5
	if (returnWithValue == 0)
		emit0("ret");
	emit0("end");
}

void icg_error(int err){
	printf("Error Number %d\n", err);
	exit(err);
}

int checkPredefined(Node* ptr){
	return 0;
}

void genLabel(char* label){
	char genlab[LABEL_SIZE];
	sprintf(genlab, "LAB%d", labelcount);
	strcpy(label, genlab);
	labelcount++;
}

void emitLabel(char* label){
	fprintf(ucodeFile, "%s%10s\n", label, "nop");
	printf("%s%10s\n", label, "nop");
}