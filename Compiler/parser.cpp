//2010305042 윤상수
#define _CRT_SECURE_NO_WARNINGS
#define PS_SIZE 200

enum noderepd{
	terminal, nonterm
};

typedef struct nodeType{
	struct tokenType token;//토큰 종류
	enum noderepd noderep; //노드 종류
	struct nodeType *son;  //왼쪽 링크
	struct nodeType *brother;//오른쪽 링크
}Node;

Node *buildNode(struct tokenType token);
Node *buildTree(int nodeNumber, int rhsLength);
Node *parser();
Node *valueStack[PS_SIZE];

void printNode(Node *pt, int indent);
void printTree(Node *pt, int indent);
int meaningfulToken(struct tokenType token);

int sp;
int stateStack[PS_SIZE];
int symbolStack[PS_SIZE];

void printToken(struct tokenType token);
void dumpStack();
void errorRecovery();

enum nodeNumber{
	ACTUAL_PARAM, ADD, ADD_ASSIGN, ARRAY_VAR, ASSIGN_OP,
	CALL, COMPOUND_ST, CONST_NODE, DCL, DCL_ITEM, DCL_LIST, DCL_SPEC, DIV, DIV_ASSIGN,
	EQ, ERROR_NODE, EXP_ST, FORAML_PARA, FUNC_DEF, FUNC_HEAD, GE, GT,
	IDENT, IF_ELSE_ST, IF_ST, INDEX, INT_NODE, LE, LOGICAL_AND, LOGICAL_NOT, LOGICAL_OR,
	LT, MOD, MOD_ASSIGN, MUL, MUL_ASSIGN, NE, NUMBER,
	PARAM_DCL, POST_DEC, POST_INC, PRE_DEC, PRE_INC, PROGRAM, RETURN_ST, SIMPLE_VAR, STAT_LIST, SUB, SUB_ASSIGN,
	UNARY_MINUS, VOID_NODE, WHILE_ST
};

char *tokenName[] = {
	"!", "!=", "%", "%=", "%ident", "%number",
	"&&", "(", ")", "*", "*=", "+",
	"++", "+=", ",", "-", "--", "-=", "/", "/=", ";",
	"<", "<=", "=", "==", ">", ">=", "[", "]", "EOF",
	"const", "else", "if", "int", "return", "void", "while",
	"{", "||", "}"
};

char *nodeName[] = { // 규칙 이름
	"ACTUAL_PARAM", "ADD", "ADD_ASSIGN", "ARRAY_VAR", "ASSIGN_OP",
	"CALL", "COMPOUND_ST", "CONST_NODE", "DCL", "DCL_ITEM", "DCL_LIST", "DCL_SPEC", "DIV", "DIV_ASSIGN",
	"EQ", "ERROR_NODE", "EXP_ST", "FORAML_PARA", "FUNC_DEF", "FUNC_HEAD", "GE", "GT",
	"IDENT", "IF_ELSE_ST", "IF_ST", "INDEX", "INT_NODE", "LE", "LOGICAL_AND", "LOGICAL_NOT", "LOGICAL_OR",
	"LT", "MOD", "MOD_ASSIGN", "MUL", "MUL_ASSIGN", "NE", "NUMBER",
	"PARAM_DCL", "POST_DEC", "POST_INC", "PRE_DEC", "PRE_INC", "PROGRAM", "RETURN_ST", "SIMPLE_VAR", "STAT_LIST", "SUB", "SUB_ASSIGN",
	"UNARY_MINUS", "VOID_NODE", "WHILE_ST"
};

int ruleName[] = { // 생성 규칙
	0, PROGRAM, 0, 0, 0, 0, FUNC_DEF, FUNC_HEAD, DCL_SPEC, 0,
	0, 0, 0, CONST_NODE, INT_NODE, VOID_NODE, 0, FORAML_PARA, 0, 0,
	0, 0, PARAM_DCL, COMPOUND_ST, DCL_LIST, DCL_LIST, 0, 0, DCL, 0,
	0, DCL_ITEM, DCL_ITEM, SIMPLE_VAR, ARRAY_VAR, 0, 0, STAT_LIST, 0, 0,
	0, 0, 0, 0, 0, 0, EXP_ST, 0, 0, IF_ST,
	IF_ELSE_ST, WHILE_ST, RETURN_ST, 0, 0, ASSIGN_OP, ADD_ASSIGN, SUB_ASSIGN, MUL_ASSIGN, DIV_ASSIGN,
	MOD_ASSIGN, 0, LOGICAL_OR, 0, LOGICAL_AND, 0, EQ, NE, 0, GT,
	LT, GE, LE, 0, ADD, SUB, 0, MUL, DIV, MOD,
	0, UNARY_MINUS, LOGICAL_NOT, PRE_INC, PRE_DEC, 0, INDEX, CALL, POST_INC, POST_DEC,
	0, 0, ACTUAL_PARAM, 0, 0, 0, 0
};

Node *parser(){
	extern int parsingTable[NO_STATES][NO_SYMBOLS + 1];
	extern int leftSymbol[NO_RULES + 1], rightLength[NO_RULES + 1];
	int entry, ruleNumber, lhs;
	int currentState;
	struct tokenType token;
	Node *ptr;

	sp = 0;
	stateStack[sp] = 0;
	token = scanner();

	while (true){
		currentState = stateStack[sp];
		entry = parsingTable[currentState][token.number];

		if (entry > 0){
			sp++;
			if (sp > PS_SIZE){
				printf("Critical Error : Parsing Stack Overflow\n");
				exit(1);
			}
			symbolStack[sp] = token.number;
			stateStack[sp] = entry;
			valueStack[sp] = meaningfulToken(token) ? buildNode(token) : NULL;
			token = scanner();
		}
		else if (entry < 0){
			ruleNumber = -entry;
			if (ruleNumber == GOAL_RULE){
				return valueStack[sp - 1];
			}
			ptr = buildTree(ruleName[ruleNumber], rightLength[ruleNumber]);
			sp = sp - rightLength[ruleNumber];
			lhs = leftSymbol[ruleNumber];
			currentState = parsingTable[stateStack[sp]][lhs];
			sp++;
			symbolStack[sp] = lhs;
			stateStack[sp] = currentState;
			valueStack[sp] = ptr;
		}
		else {
			printf(" === error in source === \n");
			printf("Current Token : ");
			printToken(token);
			dumpStack();
			errorRecovery();
			token = scanner();
		}
	}
}
//의미있는 노드인지 검사
int meaningfulToken(struct tokenType token){
	if ((token.number == tident) || (token.number == tnumber)){
		//명칭이거나 상수이면 1 리턴
		return 1;
	}
	else return 0;
}
//의미있는 terminal 심벌일 경우 단말 노드 생성
Node *buildNode(struct tokenType token){
	Node *ptr;
	ptr = (Node *)malloc(sizeof(Node));

	if (!ptr){
		printf("malloc error in buildNode()\n");
		exit(1);
	}
	ptr->token = token;
	ptr->noderep = terminal;
	ptr->son = ptr->brother = NULL;
	return ptr;
}
//의미있는 생성 규칙이면 서브트리 생성
Node *buildTree(int nodeNumber, int rhsLength){
	int i, j, start;
	Node *first, *ptr;

	i = sp - rhsLength + 1;

	while ((i <= sp) && (valueStack[i] == NULL)){
		i++;
	}
	if (!nodeNumber&&i > sp){
		return NULL;
	}
	start = i;

	while (i <= sp - 1){
		j = i + 1;
		while ((j <= sp) && (valueStack[j] == NULL)){
			j++;
		}
		if (j <= sp){
			ptr = valueStack[i];
			while (ptr->brother){
				ptr = ptr->brother;
			}
			ptr->brother = valueStack[j];
		}
		i = j;
	}
	first = (start > sp) ? NULL : valueStack[start];

	if (nodeNumber){
		ptr = (Node *)malloc(sizeof(Node));
		if (!ptr){
			printf("malloc error in buildTree()\n");
			exit(1);
		}
		ptr->token.number = nodeNumber;
		//ptr->token.value.num=NULL;
		ptr->noderep = nonterm;
		ptr->son = first;
		ptr->brother = NULL;
		return ptr;
	}
	else return first;
}
//AST 출력 시 각 노드의 정보 출력
void printNode(Node *pt, int indent){
	int i;
	extern FILE* astFile;

	for (i = 1; i <= indent; i++){
		fprintf(astFile," ");
	}
	if (pt->noderep == terminal){
		if (pt->token.number == tident){
			fprintf(astFile," Terminal: %s", pt->token.value.id);
		}
		else if (pt->token.number == tnumber){
			fprintf(astFile, " Terminal: %d", pt->token.value.num);
		}
	}
	else {
		int i;
		i = (int)(pt->token.number);
		fprintf(astFile, " Nonterminal: %s", nodeName[i]);
	}
	fprintf(astFile, "\n");

}
//AST 출력
void printTree(Node *pt, int indent){
	Node *p = pt;
	while (p != NULL){
		printNode(p, indent);
		if (p->noderep == nonterm)
			printTree(p->son, indent + 3);
		p = p->brother;
	}
}

void printToken(struct tokenType token) {
	if (token.number == tident)
		printf("%s", token.value.id);
	else if (token.number == tnumber)
		printf("%d", token.value.num);
	else
		printf("%s", tokenName[token.number]);
}

void dumpStack() {
	int i, start;
	if (sp > 10)	start = sp - 10;
	else			start = 0;

	printf("\n *** dump state stack : ");
	for (i = start; i <= sp; ++i)
		printf(" %d", stateStack[i]);

	printf(" \n *** dump symbol stack : ");

	for (i = start; i <= sp; ++i)
		printf(" %d", symbolStack[i]);

	printf(" \n");
}

void errorRecovery() {
	struct tokenType tok;
	int parenthesiscount, bracecount;
	int i;

	parenthesiscount = bracecount = 0;
	while (true) {
		tok = scanner();
		if (tok.number == teof)			exit(1);
		if (tok.number == tlparen)		parenthesiscount++;
		else if (tok.number == trparen)	parenthesiscount--;
		if (tok.number == tlbrace)		bracecount++;
		else if (tok.number == trbrace)	bracecount--;

		if ((tok.number == tsemicolon) && (parenthesiscount <= 0) && (bracecount <= 0))
			break;
	}

	for (i = sp; i >= 0; i--) {
		if (stateStack[i] == 36) break;
		if (stateStack[i] == 24) break;
		if (stateStack[i] == 25) break;
		if (stateStack[i] == 17) break;
		if (stateStack[i] == 2) break;
		if (stateStack[i] == 0) break;
	}
	sp = i;
}