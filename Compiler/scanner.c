// 2010305042 윤상수
#define _CRT_SECURE_NO_WARNINGS
#define ID_LENGTH 12
#define NO_KEYWORDS 7

enum tsymbol{	
// total(41) = Special Symbols(30) + Reserved Symbols(7) + ETC(tnull, tident, tnumber, teof)
	tnull = -1, tnot, tnotequ, tmod, tmodAssign, tident, tnumber, tand, tlparen, trparen,
	tmul, tmulAssign, tplus, tinc, taddAssign, tcomma, tminus, tdec, tsubAssign,

	tdiv, tdivAssign, tsemicolon, tless, tlesse, tassign, tequal, tgreat, tgreate, tlbracket, trbracket,
	teof, tconst, telse, tif, tint, treturn, tvoid, twhile, tlbrace, tor, trbrace
};
struct tokenType{
	int number;
	union{
		char id[ID_LENGTH];
		int num;
	}value;
};
char*keyword[] = { "const", "else", "if", "int", "return", "void", "while" };
enum tsymbol tnum[] = { tconst, telse, tif, tint, treturn, tvoid, twhile };
int k;				// 지정어 위치 저장을 위해 사용
int isDelimiter;	// 연산자인지 구분자인지 분간하기 위해 사용
extern FILE* sourceFile;

struct tokenType scanner(){
	struct tokenType token;
	
	int i, j, num;
	char ch, id[ID_LENGTH];

	// 초기화
	isDelimiter = 0;
	token.number = tnull;
	do{
		while (isspace(ch = getc(sourceFile))){
			printf("");
		}

		if (isalpha(ch)){		// 첫 글자가 알파벳인 경우(지정어, 명칭)
			i = 0;

			do{					// 문자형 배열 변수 id에 검사할 값을 쪼개어 입력
				if (i < ID_LENGTH)	id[i++] = ch;
				ch = getc(sourceFile);
			} while (isalnum(ch));	// 알파벳 혹은 숫자인 경우 

			id[i] = '\0';
			ungetc(ch, sourceFile);
			// 초기화
			i = 0;
			j = NO_KEYWORDS - 1;

			do{							// 값이 지정어인지 검색하는 이진 탐색
				k = (i + j) / 2;		// k값(중간값) 초기화
				if (strcmp(id, keyword[k]) >= 0)	i = k + 1; // 전위 탐색
				if (strcmp(id, keyword[k]) <= 0)	j = k - 1; // 후위 탐색
			} while (i <= j);

			if ((i - 1) > j){			// 지정어인 경우
				token.number = tnum[k];	// token 번호에 지정어임을 인지
				strcpy(token.value.id, id);	// token 값에 id값을 저장
			}
			else{						// 명칭인 경우
				token.number = tident;	// token 번호에 명칭임을 인지
				strcpy(token.value.id, id);	// token 값에 id값을 저장
			}
		}
		else if (isdigit(ch)){		// 첫 글자가 숫자인 경우(상수)
			num = 0;				// 초기화

			do{						// char형 값을 int형으로 변환하여 저장
				num = 10 * num + (int)(ch - '0');
				ch = getc(sourceFile);		// 다음 문자값 로딩
			} while (isdigit(ch));	// 입력값이 숫자인 동안 반복

			ungetc(ch, sourceFile);		// 입력 포인터를 한칸 뒤로 옮김
			token.number = tnumber;	// token 번호에 상수임을 인지
			token.value.num = num;
		}
		else{						// 첫 글자가 알파벳, 상수 이외의 값인 경우(연산자, 구분자)
			switch (ch){			
			case '/':
				ch = getc(sourceFile);

				if (ch == '*'){
					strcpy(token.value.id, "/*");
					do{
						while (ch != '*'){
							ch = getc(sourceFile);
						}
						ch = getc(sourceFile);
					} while (ch != '/');
				}
				else if (ch == '/'){
					strcpy(token.value.id, "//");
					while (getc(sourceFile) != '\n');
				}
				else if (ch == '='){
					strcpy(token.value.id, "/=");
					token.number = tdivAssign;
				}
				else{
					strcpy(token.value.id, "/");
					token.number = tdiv;
					ungetc(ch, sourceFile);
				}
				break;

			case '!':
				ch = getc(sourceFile);

				if (ch == '='){
					strcpy(token.value.id, "!=");
					token.number = tnotequ;
				}
				else{
					strcpy(token.value.id, "!");
					token.number = tnot;
					ungetc(ch, sourceFile);
				}
				break;

			case '%':
				ch = getc(sourceFile);

				if (ch == '='){
					strcpy(token.value.id, "%=");
					token.number = tmodAssign;
				}
				else{
					strcpy(token.value.id, "%");
					token.number = tmod;
					ungetc(ch, sourceFile);
				}
				break;

			case '&':
				ch = getc(sourceFile);

				if (ch == '&'){
					strcpy(token.value.id, "&&");
					token.number = tand;
				}
				else{
					strcpy(token.value.id, "");
					ungetc(ch, sourceFile);
				}
				break;

			case '*':
				ch = getc(sourceFile);

				if (ch == '='){
					strcpy(token.value.id, "*=");
					token.number = tmulAssign;
				}
				else{
					strcpy(token.value.id, "*");
					token.number = tmul;
					ungetc(ch, sourceFile);
				}
				break;

			case '+':
				ch = getc(sourceFile);

				if (ch == '+'){
					strcpy(token.value.id, "++");
					token.number = tinc;
				}
				else if (ch == '='){
					strcpy(token.value.id, "+=");
					token.number = taddAssign;
				}
				else{
					strcpy(token.value.id, "+");
					token.number = tplus;
					ungetc(ch, sourceFile);
				}
				break;

			case '-':
				ch = getc(sourceFile);

				if (ch == '-'){
					strcpy(token.value.id, "--");
					token.number = tdec;
				}
				else if (ch == '='){
					strcpy(token.value.id, "-=");
					token.number = tsubAssign;
				}
				else{
					strcpy(token.value.id, "-");
					token.number = tminus;
					ungetc(ch, sourceFile);
				}
				break;

			case '<':
				ch = getc(sourceFile);

				if (ch == '='){
					strcpy(token.value.id, "<=");
					token.number = tlesse;

				}
				else{
					strcpy(token.value.id, "<");
					token.number = tless;
					ungetc(ch, sourceFile);
				}
				break;

			case '=':
				ch = getc(sourceFile);

				if (ch == '='){
					strcpy(token.value.id, "==");
					token.number = tequal;
				}
				else{
					strcpy(token.value.id, "=");
					token.number = tassign;
					ungetc(ch, sourceFile);
				}
				break;

			case '>':
				ch = getc(sourceFile);

				if (ch == '='){
					strcpy(token.value.id, ">=");
					token.number = tgreate;
				}
				else{
					strcpy(token.value.id, ">");
					token.number = tgreat;
					ungetc(ch, sourceFile);
				}
				break;

			case '|':
				ch = getc(sourceFile);

				if (ch == '|'){
					strcpy(token.value.id, "||");
					token.number = tor;
				}
				else{
					strcpy(token.value.id, "");
					ungetc(ch, sourceFile);
				}
				break;

			case '(':
				strcpy(token.value.id, "(");
				token.number = tlparen;
				isDelimiter = 1;
				break;

			case ')':
				strcpy(token.value.id, ")");
				token.number = trparen;
				isDelimiter = 1;
				break;

			case ',':
				strcpy(token.value.id, ",");
				token.number = tcomma;
				isDelimiter = 1;
				break;

			case ';':
				strcpy(token.value.id, ";");
				token.number = tsemicolon;
				isDelimiter = 1;
				break;

			case '[':
				strcpy(token.value.id, "[");
				token.number = tlbracket;
				isDelimiter = 1;
				break;

			case ']':
				strcpy(token.value.id, "]");
				token.number = trbracket;
				isDelimiter = 1;
				break;

			case '{':
				strcpy(token.value.id, "{");
				token.number = tlbrace;
				isDelimiter = 1;
				break;

			case '}':
				token.number = trbrace;
				strcpy(token.value.id, "}");
				isDelimiter = 1;
				break;

			case EOF:
				token.number = teof;
				break;

			default:	// 알 수 없는 항목의 경우
				printf("Current character : %c\n", ch);
				break;
			}
		}
	} while (token.number == tnull);

	return token;	// token 값 반환
}